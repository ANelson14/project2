package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LogoutTest {
	public BankLogin bl;
	public Bank b;
	@Given("a user is logged in at the bank page wanting to logout {string} {string}")
	public void a_user_is_logged_in_at_the_bank_page_wanting_to_logout(String string, String string2) {
		bl = new BankLogin(BankDriverUtility.driver);
		b = new Bank(BankDriverUtility.driver);
		bl.username.sendKeys(string);
		bl.password.sendKeys(string2);
		bl.loginButton.click();
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));	}



	@When("a user clicks the logout link")
	public void a_user_clicks_the_logout_link() {
		b.logoutLink.click();
	}
	@Then("they are redirected back to the login page")
	public void they_are_redirected_back_to_the_login_page() {
	    WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/user/login"));
	    assertEquals("http://localhost:4200/user/login", BankDriverUtility.driver.getCurrentUrl());

	}


}
