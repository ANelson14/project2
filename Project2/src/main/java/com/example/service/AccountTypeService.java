package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Account;
import com.example.model.AccountType;
import com.example.repository.AccountRepository;
import com.example.repository.AccountTypeRepository;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class AccountTypeService {

	private AccountTypeRepository atRepo;
	private AccountRepository aRepo;
	
	public AccountType getByClientId(String id) {
		Account a = aRepo.findByAccountNumber(id);
		//System.out.println(a);
		return atRepo.findByaccList(a).get(0);

	}

	public AccountType getById(int acTypeId) {
		AccountType actype = atRepo.findByTypeId(acTypeId);
		return actype;
	}

}
