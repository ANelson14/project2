package com.example.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.model.ClientUser;
import com.example.repository.ClientUserRepository;

@SpringBootTest
public class CServiceTest {

	@Mock
	private ClientUserRepository cuRepo;
	
	@InjectMocks
	private ClientUserService clientServ;
	
	private ClientUser cu;
	@InjectMocks
	ClientUserService clServ = mock(ClientUserService.class);
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		cu = new ClientUser("Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888",
				"starjohnson", "password", null, null);
		when(cuRepo.findByEmail("starjohnson@maildrop.cc")).thenReturn(cu);
		when(cuRepo.findByUsername("starjohnson")).thenReturn(cu);
		when(cuRepo.findByEmail("nope")).thenReturn(null);
		
	}
	@Test
	public void getClientByUsernameSuccess() {
		assertEquals(clientServ.getClientByUsername("starjohnson"), cu);
	}
	@Test
	public void getClientByUsernameFailure() {
		assertEquals(clientServ.getClientByUsername("nope"), null);
	}
	@Test
	public void verifyLoginSuccess() {
		assertEquals(clientServ.verify("starjohnson", "password"),true);
	}
	@Test
	public void verifyLoginFailure() {
		assertEquals(clientServ.verify("nope", "password"),false);
	}
	@Test
	public void verifyPasswordEmailSuccess() {

		doNothing().when(clServ).sendPasswordEmail("starjohnson@maildrop.cc");
		clServ.sendPasswordEmail("starjohnson@maildrop.cc");
		verify(clServ, times(1)).sendPasswordEmail("starjohnson@maildrop.cc");
		
	}
	
	
	
}
