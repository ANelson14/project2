package com.example.gluecode;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;
import com.example.e2e.page.Statement;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StatementTest {

	public BankLogin bl;
	public Bank b;
	public Statement s;
	@Given("a user is at the bank page wanting to go to statement {string} {string}")
	public void a_user_is_at_the_bank_page_wanting_to_go_to_statement(String string, String string2) {
		bl = new BankLogin(BankDriverUtility.driver);
		b = new Bank(BankDriverUtility.driver);
		s = new Statement(BankDriverUtility.driver);
		bl.username.sendKeys(string);
		bl.password.sendKeys(string2);
		bl.loginButton.click();
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));
	}

	
	@When("a user clicks the statement link")
	public void a_user_clicks_the_statement_link() {
		b.statementLink.click();
	}
	@When("they are redirected to the statement page")
	public void they_are_redirected_to_the_statement_page() {
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/statement"));
	}
	@When("then a user selects their account {string}")
	public void then_a_user_selects_their_account(String string) {
		s.getSelectOptions().selectByValue(string);
	}
	
	@Then("then submits the statement information")
	public void then_submits_the_statement_information() {
	s.subButton.click();
	}
}
