Feature: Bank Application Balance View
	As a User, I wish to View My Account Balances 
	
	Scenario Outline: View Balances
		Given a user is logged in at the bank page wanting to go to Balance with "<username>" "<password>"
		When a user clicks the balance link
		Then they are redirected to the balance page

	Examples:
		| username     | password  |
		| johnlao      | password  |
		| leooo        | password  | 
		| starjohnson  | password  |