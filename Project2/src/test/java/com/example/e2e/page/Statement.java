package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Statement {
	@FindBy(xpath = "//*[@id='accountNumber']")
	public WebElement account;

	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-statement/div[2]/button")
	public WebElement subButton;

	public Select getSelectOptions() {
		return new Select(account);
	}

	public Statement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
