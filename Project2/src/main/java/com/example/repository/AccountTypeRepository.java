package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Account;
import com.example.model.AccountType;

@Repository
public interface AccountTypeRepository extends JpaRepository<AccountType, Integer>{

	public List<AccountType> findAll();

	public List<AccountType> findByaccList(Account a);
	public AccountType findByTypeId(int id);
//	@EntityGraph(value = "ClientUser.accList", type = EntityGraphType.FETCH)
//	public ClientUser findByUsername(String username);
	
}
