# this is a comment
Feature: 1Bank Application Register
	As a User, I wish to register to banking application 
	
	Scenario Outline: Register to bank applciation as a user
		Given a user is at the login page of bank application
		When a user click register button
		And a user inputs first name "<firstName>"
		And then a user inputs last name "<lastName>"
		And then a user inputs contact Number  "<contactNumber>"
		And then a user inputs address  "<address>"
		And then a user inputs age  "<age>"
		And then a user inputs username "<username>"
		And then a user inputs password  "<password>"
		And then a user inputs email "<email>"
		And then a user check the checkbox to agree to terms
		But then submits the information
		Then the user is redirected to the login page

	Examples:
		| firstName | lastName | contactNumber | address                     | age | username  | password  | email                | 
		| john      | lao      | 224-243-3333  | 333 Paula St, sunnyvale, CA | 24  | jlao52    | password1 | johnlao@gmail.com    |
		| henry     | zheng    | 236-243-3333  | 235 Howard St, San Jose, CA | 26  | hzheng88  | password2 | henryzheng@gmail.com |
		| alex 		| huang    | 224-243-5821  | 255 Linclon St, Urbana, IL  | 33  | huang555  | password3 | alexhuang@gmail.com  |