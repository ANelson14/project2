package com.example.controller;

import java.util.LinkedHashMap;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mailutils.JavaMailUtils;
import com.example.model.ClientUser;
import com.example.service.ClientUserService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin(origins = "http://revatureproject2onlinebankingwebsite.s3-website.us-east-2.amazonaws.com")
//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value="/api/users")
@AllArgsConstructor(onConstructor=@__(@Autowired))

@NoArgsConstructor
public class ClientUserController {
	
	private ClientUserService clientServ;
	

	@PostMapping(value="/register")
	public ResponseEntity<String> insertUser(@RequestBody LinkedHashMap<String,String> fMap) {
		String username = fMap.get("username");
		String firstName = fMap.get("firstName");
		String lastName = fMap.get("lastName");
		String address = fMap.get("address");
		int age = Integer.parseInt(fMap.get("age"));
		String emailAddress = fMap.get("email");
		String contactNumber = fMap.get("contactNumber");
		String password = fMap.get("password");
		String photoUrl = fMap.get("photoUrl");
		 
				System.out.println(emailAddress);
		/*
		 * { "username": "john", "firstName": "john", "lastName": "lao", "address":
		 * "Santa Paula", "age": "10", "emailAddress": "laojohnmatthew@gmail.com",
		 * "contactNumber": "2246237364" }
		 */
	
		ClientUser cu = new ClientUser(firstName, lastName, address, age, emailAddress, contactNumber, username, password, photoUrl, null);
		clientServ.insertClient(cu);
		try {
			JavaMailUtils.sendAsHtml(cu.getEmail(), "User Successfully created", "<h2>User Successfully Created</h2><p> Hello" + cu.getUsername() + ". You have successfully registered with the online banking system</p>\"");
	} catch (MessagingException e) {
			
			e.printStackTrace();
		}
		System.out.println(cu);
		//clientServ.insertClient(cu);
		return new ResponseEntity<>("resource was created", HttpStatus.CREATED);
	}

	@PostMapping("/login")
	public ResponseEntity<ClientUser> verifyLogin(@RequestBody LinkedHashMap<String, String> fMap)
	{
		ClientUser user = null;
		if(clientServ.verify(fMap.get("username"), fMap.get("password")))
		{
			user = clientServ.getClientByUsername(fMap.get("username"));
		
		return new ResponseEntity<>(user, HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(user, HttpStatus.BAD_REQUEST);

	}
	
	@GetMapping("/{username}")
	public ResponseEntity<ClientUser> getClientUserByUsername(@PathVariable("username") String username) {
		ClientUser cu = clientServ.getClientByUsername(username);
		if(cu == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(cu, HttpStatus.OK);
	}
	
	@PutMapping("/bank/profile/updateprofile")
	public ResponseEntity<String> updateClient(@RequestBody LinkedHashMap<String,String> updateMap){
		System.out.println("update :::" +updateMap);
		String firstName = (String)updateMap.get("firstName");
		String lastName = (String)updateMap.get("lastName");
		String address = (String)updateMap.get("address");
		String emailAddress = updateMap.get("email");
		String contactNumber = (String)updateMap.get("contactNumber");
		
		int clientId = Integer.parseInt(updateMap.get("clientId"));
		ClientUser old = clientServ.getClientByClientId(clientId);
		ClientUser cu;
		if(contactNumber.equals("")) {
			 cu = new ClientUser(clientId, old.getUsername(),old.getPassword(), firstName, lastName, address, old.getAge(),emailAddress, old.getContactNumber(),  old.getPhotoUrl(), old.getAccList());

		}
		else {
		 cu = new ClientUser(clientId, old.getUsername(),old.getPassword(), firstName, lastName, address, old.getAge(),emailAddress, contactNumber,  old.getPhotoUrl(), old.getAccList());
		}
		clientServ.updateClient(cu);
		return new ResponseEntity<>("Profile Updated Successfylly", HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/forgotpassword")
	public ResponseEntity<String> getPasswordByEmail(@RequestBody LinkedHashMap<String,String> passMap)
	{
		System.out.println(passMap);
	   clientServ.sendPasswordEmail(passMap.get("email"));
	  return new ResponseEntity<>("Email has been sent", HttpStatus.ACCEPTED); 
	}
	
	//NEEDS A TEST
	@PostMapping("/updateToken")
	public ResponseEntity<ClientUser> updateToken(@RequestBody LinkedHashMap<String, String> fMap) {
		ClientUser user = null;
		if(clientServ.verify(fMap.get("username"), fMap.get("password")))
		{
			user = clientServ.getClientByUsername(fMap.get("username"));
		
		return new ResponseEntity<>(user, HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(user, HttpStatus.BAD_REQUEST);
	}

}
