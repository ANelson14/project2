package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "Client_User")
public class ClientUser {

	@Id
	@Column(name = "client_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private int clientId;

	@Column(name = "username", nullable = false, unique = true)
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "address")
	private String address;

	@Column(name = "age")
	private int age;

	@Column(name = "email_address")
	private String email;

	@Column(name = "contact_Number")
	private String contactNumber;

	@Lob
	@Column(name = "photo", columnDefinition = "BLOB")
	private String photoUrl;

	@OneToMany(mappedBy = "cHolder", fetch = FetchType.EAGER)
	private List<Account> accList = new ArrayList<>();

	public ClientUser(String firstName, String lastName,  String address, int age, String emailAddress,
			String contactNumber,String username, String password, String photoUrl, List<Account> accList) {

		super();
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.age = age;
		this.email = emailAddress;
		this.contactNumber = contactNumber;
		this.photoUrl = photoUrl;
		this.accList = accList;
	}

	@Override
	public String toString() {
		return "ClientUser [clientId=" + clientId + ", username=" + username + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + ", address=" + address + ", age=" + age + ", emailAddress="
				+ email + ", contactNumber=" + contactNumber + ", photoUrl=" + photoUrl + ", accList=" + accList
				+ "]";
	}

}
