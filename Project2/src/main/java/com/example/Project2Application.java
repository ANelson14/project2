package com.example;
 
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.model.Transaction;
import com.example.repository.AccountRepository;
import com.example.repository.AccountTypeRepository;
import com.example.repository.BranchRepository;
import com.example.repository.ClientUserRepository;
import com.example.service.AccountService;
import com.example.service.TransactionService;

import lombok.AllArgsConstructor;

@SpringBootApplication

@AllArgsConstructor
public class Project2Application implements ApplicationRunner {
	private ClientUserRepository clientUserRepository;
	private BranchRepository branchRepository;
	private AccountTypeRepository accountTypeRepository;
	private AccountRepository accountRepository;
	private AccountService aService;
	private TransactionService tService;

	public static void main(String[] args) {

		SpringApplication.run(Project2Application.class, args);
	}

	public void insertInitialValues() {
		clientUserRepository.saveAll(Arrays.asList(
		          new ClientUser("john","lao","santa paula", 23 ,"laojohnmatthew@gmail.com", "2246237364", "johnlao", "password", null, null), new ClientUser("john","leo","santa paula", 24 ,"leojohnmatthew@gmail.com", "2246237364", "leooo","password", null, null)));
		    	//branchRepository.saveAll(Arrays.asList( new Branch("Santa Clara"), new Branch("Houston"), new Branch("San Francisco")));
		    	//accountTypeRepository.saveAll(Arrays.asList( new AccountType("checking"), new AccountType("saving")));
		    	ClientUser c = new ClientUser("Star", "Johnson", "Space", 55,"starjohnson@maildrop.cc","5015558888", "starjohnson","password", null, null); clientUserRepository.save(c); 
		    	
		    AccountType at1 = new AccountType("checking");
		    AccountType at2 = new AccountType("savings");
		    accountTypeRepository.save(at1);
		    accountTypeRepository.save(at2);
		    Branch b1 =  new Branch("Santa Clara");
		    Branch b2 = new Branch("Houston");
		    Branch b3 = new Branch("San Francisco");
		    branchRepository.save(b1);
		    branchRepository.save(b2);
		    branchRepository.save(b3);
		    
		    	List<Transaction> t = new ArrayList<>();
				Account a3 = new Account("132548103", 0, c, at2, b3, t);  
				  accountRepository.save(a3);
		    	Account a = new Account("132548101", 5, c, at1, b1, t); // not auto generated for account no yet.
				  accountRepository.save(a); 				  
				  Account a2 = new Account("132548102", 50000000, c, at2, b3, t); // not auto generated for account no yet. //
				  
				  double deposit = 200;
				  double currBalance = a2.getBalance() + deposit;
				  a2.setBalance(currBalance);
				  Transaction t1 = new Transaction("Payday", new Timestamp(System.currentTimeMillis()), 0, 200, currBalance, a2);
				  double withdrawal = 280;
				  currBalance = a2.getBalance() - withdrawal;
				  Transaction t2 = new Transaction("Purchase of Amazing Figure", new Timestamp(System.currentTimeMillis()), 280, 0,currBalance, a2);
				  t.add(t1);
				  t.add(t2);
				  accountRepository.save(a2);
			
		    	aService.transferFunds("132548102", "132548101",  5000.02);
		    	List<Transaction>  tList1 = tService.getAllTransactionsByClientId("132548102");
		    	System.out.println(tList1);
		    	System.out.println();
		    	List<Transaction>  tList21 = tService.getAllTransactionsByClientId("132548101");
		    	System.out.println(tList21);
//		    	System.out.println();
		// System.out.println(accountRepository.findByAccountNumber("132548101"));
		// System.out.println(accountRepository.findByAccountNumber("132548102"));

//		    	aService.transferFunds("132548102", "132548101",  500000.02);
		/*
		 * List<Transaction> tList3 =
		 * tService.getAllTransactionsByClientId("132548102");
		 * System.out.println(tList3); System.out.println(); List<Transaction> tList4 =
		 * tService.getAllTransactionsByClientId("132548101");
		 * System.out.println(tList4); System.out.println();
		 */
//		    	System.out.println(accountRepository.findByAccountNumber("132548101"));
//		    	System.out.println(accountRepository.findByAccountNumber("132548102"));
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {

//	insertInitialValues();

	}

}
