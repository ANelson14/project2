package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Account;
import com.example.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer>{

//	public List<Transaction> findByaccHolder(Account account);
	public List<Transaction> findByAccount(Account account);
	
	public List<Transaction> findAll();
	// not sure whether this should be in transDao or AccountDao
//	@EntityGraph(value = "Account.transList", type = EntityGraphType.FETCH)
//	public Account findByAccountNumber(String accountNumber);
//	
//	
}



